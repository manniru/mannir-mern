# BISMILLAHIR RAHAMANIR RAHIM
# Developed By: Muhammad Mannir Ahmad (manniru@gmail.com)
# Created on December 8, 2018  9:19:30 AM 

FROM ubuntu:18.04
MAINTAINER Muhammad Mannir Ahmad "manniru@gmail.com"

RUN apt-get update
RUN apt-get install -y python python-pip wget
RUN pip install Flask

ADD hello.py /home/hello.py

WORKDIR /home